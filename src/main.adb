with Ada.Command_Line;         use Ada.Command_Line;
with Ada.Strings.Unbounded;    use Ada.Strings.Unbounded;
with Ada.Text_IO;              use Ada.Text_IO;
with Ada.Text_IO.Unbounded_IO; use Ada.Text_IO.Unbounded_IO;

with Plaid;
with Plaid.St;
with Plaid.St.Ast;
with Plaid.St.Scanner;
with Plaid.St.Parser;

procedure Main is

   procedure Run (Source : Unbounded_String) is
   begin
      Put_Line ("Source is");
      Put_Line (Source);

      declare
         Scanner_Inst : Plaid.St.Scanner.Scanner_Instance := Plaid.St.Scanner.New_Scanner (Source);
      begin
         Plaid.St.Scanner.Scan_Tokens (Scanner_Inst);
         declare
            Tokens : constant Plaid.St.Scanner.Token_Vectors.Vector :=
              Plaid.St.Scanner.Tokens (Scanner_Inst);
            --  Parser_Inst : Plaid.St.Parser.Parser_Instance
            --    := Plaid.St.Parser.New_Parser (Tokens);
            --  Expr        : Plaid.St.Ast.Expr_Access := Plaid.St.Parser.Parse (Parser_Inst);
         begin
            for E of Tokens loop
               Put_Line (Plaid.St.Scanner.Token_Img (E));
            end loop;
            null;
         end;
      end;
   end Run;

   procedure Run_File (Filename : String) is
      F      : File_Type;
      Source : Unbounded_String;
   begin
      Open (F, In_File, Filename);
      while not End_Of_File (F) loop
         declare
            Line : constant Unbounded_String := Get_Line (F);
         begin
            Append (Source, Line & ASCII.LF);
         end;
      end loop;
      Close (F);

      Run (Source);
   end Run_File;

   procedure Run_Prompt is
   begin
      Put_Line ("No interactive Transcript (YET)");
   end Run_Prompt;

begin
   if Argument_Count > 1 then
      Put_Line ("Usage: Plaid.St [script]");
      Set_Exit_Status (64);
      return;
   elsif Argument_Count = 1 then
      Run_File (Argument (1));
   else
      Run_Prompt;
   end if;
end Main;
