package body Plaid.St.Ast.Expr is

   procedure Create_Binary
     (Left : Expr_Access; Operator : Token; Right : Expr_Access; Result : out Binary_Access)
   is
   begin
      Result          := new Binary;
      Result.Left     := Left;
      Result.Operator := Operator;
      Result.Right    := Right;
   end Create_Binary;

   procedure Create_Unary (Operator : Token; Right : Expr_Access; Result : out Unary_Access) is
   begin
      Result          := new Unary;
      Result.Operator := Operator;
      Result.Right    := Right;
   end Create_Unary;

   procedure Create_Literal (Value : St_Object_Access; Result : out Literal_Access) is
   begin
      Result       := new Literal;
      Result.Value := Value;
   end Create_Literal;

   procedure Create_Grouping (Expression : Expr_Access; Result : out Grouping_Access) is
   begin
      Result            := new Grouping;
      Result.Expression := Expression;
   end Create_Grouping;

end Plaid.St.Ast.Expr;
