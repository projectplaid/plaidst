package body Plaid.St.Ast is

   overriding procedure Accept_Node (Item : Literal) is
   begin
      null;
   end Accept_Node;

   overriding procedure Visit_Node (Item : Literal) is
   begin
      null;
   end Visit_Node;

end Plaid.St.Ast;
