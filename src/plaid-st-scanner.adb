with Ada.Text_IO; use Ada.Text_IO;
with Ada.Long_Integer_Text_IO;

package body Plaid.St.Scanner is

   function Token_Img (T : Token) return String is
      Suffix : Unbounded_String;
      Str    : constant String :=
        T.Token_Type'Image & ":" & T.Line'Image & " [" & To_String (T.Lexeme) & "] " &
        T.Literal_Kind'Image;
   begin
      case T.Literal_Kind is
         when STRING_LITERAL =>
            Suffix := T.Literal_String;
         when INTEGER_LITERAL =>
            Suffix := To_Unbounded_String (T.Literal_Int'Img);
         when FLOAT_LITERAL =>
            Suffix := To_Unbounded_String (T.Literal_Float'Img);
         when CHARACTER_LITERAL =>
            Suffix := To_Unbounded_String ("'" & T.Literal_Char & "'");
         when NO_LITERAL =>
            Suffix := To_Unbounded_String ("");
      end case;

      if Suffix /= "" then
         return Str & " = " & To_String (Suffix);
      else
         return Str;
      end if;
   end Token_Img;

   function New_Scanner (Source : Unbounded_String) return Scanner_Instance is
      Tokens : Token_Vectors.Vector;
      Result : constant Scanner_Instance :=
        (Tokens => Tokens, Source => Source, Start => 1, Current => 1, Line => 1);
   begin
      return Result;
   end New_Scanner;

   procedure Scan_Tokens (Self : in out Scanner_Instance) is
   begin
      while not Is_At_End (Self) loop
         Self.Start := Self.Current;

         Scan_Token (Self);
      end loop;

      --  Make sure to increment this by one, we don't need the EOF token to have a lexeme
      Self.Start := Self.Current;
      Add_Token (Self, EOF);
   end Scan_Tokens;

   function Tokens (Self : Scanner_Instance) return Token_Vectors.Vector is
   begin
      return Self.Tokens;
   end Tokens;

   procedure Scan_Token (Self : in out Scanner_Instance) is
      Unused : Character := ASCII.NUL;
   begin
      declare
         C : constant Character := Advance (Self);
      begin
         case C is
            when '#' =>
               --  Other symbols
               Add_Token (Self, HASH);
            when ':' =>
               Add_Token (Self, COLON);
            when '^' =>
               Add_Token (Self, CARET);
            when '(' =>
               Add_Token (Self, LEFT_PAREN);
            when ')' =>
               Add_Token (Self, RIGHT_PAREN);
            when '{' =>
               Add_Token (Self, LEFT_BRACE);
            when '}' =>
               Add_Token (Self, RIGHT_BRACE);
            when '.' =>
               Add_Token (Self, DOT);
            when ';' =>
               Add_Token (Self, SEMICOLON);
            when '$' =>
               Add_Token_Character (Self, CHARACTER_TOKEN, Advance (Self));

            when '"' =>
               --  Comments
               while Peek (Self) /= '"' and then not Is_At_End (Self) loop
                  declare
                     Unused : Character := Advance (Self);
                     pragma Unused (Unused);
                  begin
                     null;
                  end;
               end loop;

            when ' ' =>
               --  Whitespace
               null;
            when ASCII.HT =>
               null;
            when ASCII.CR =>
               null;
            when ASCII.LF =>
               Self.Line := Self.Line + 1;

            when ''' =>
               --  Strings
               Scan_String (Self);
            when others =>
               --  Everything else
               if C = '-' and then Is_Digit (Peek (Self)) then
                  --  Numbers starting with - are a special case. If
                  --  we don't check for them first, they'll get flagged
                  --  as binary selector characters below
                  Scan_Number (Self);
               elsif Is_Binary_Selector_Char (C) then
                  --  Binary selector character
                  Unused := Advance (Self);

                  while Is_Binary_Selector_Char (Peek (Self)) loop
                     Unused := Advance (Self);
                  end loop;

                  Add_Token (Self, BINARY_SELECTOR);
               elsif Is_Digit (C) then
                  --  Must be a number
                  Scan_Number (Self);
               elsif Is_Alpha (C) then
                  --  Keyword or identifier
                  Scan_Identifier (Self);
               else
                  raise Unexpected_Char;
               end if;
         end case;
      end;
   end Scan_Token;

   procedure Add_Token (Self : in out Scanner_Instance; T : Token_Types) is
      Str : constant Unbounded_String   :=
        Unbounded_Slice (Self.Source, Self.Start, Self.Current - 1);
      Tok : constant Token (NO_LITERAL) :=
        (Literal_Kind => NO_LITERAL, Token_Type => T, Lexeme => Str, Line => Self.Line);
   begin
      Self.Tokens.Append (Tok);
   end Add_Token;

   procedure Add_Token_String
     (Self : in out Scanner_Instance; T : Token_Types; Str : Unbounded_String)
   is
      Tok : constant Token (STRING_LITERAL) :=
        (Literal_Kind   => STRING_LITERAL, Token_Type => T, Lexeme => Str, Line => Self.Line,
         Literal_String => Str);
   begin
      Self.Tokens.Append (Tok);
   end Add_Token_String;

   procedure Add_Token_Integer (Self : in out Scanner_Instance; T : Token_Types; I : Long_Integer)
   is
      Tok : constant Token (INTEGER_LITERAL) :=
        (Literal_Kind => INTEGER_LITERAL, Token_Type => T, Lexeme => To_Unbounded_String (""),
         Line         => Self.Line, Literal_Int => I);
   begin
      Self.Tokens.Append (Tok);
   end Add_Token_Integer;

   procedure Add_Token_Float (Self : in out Scanner_Instance; T : Token_Types; F : Long_Float) is
      Tok : constant Token (FLOAT_LITERAL) :=
        (Literal_Kind => FLOAT_LITERAL, Token_Type => T, Lexeme => To_Unbounded_String (""),
         Line         => Self.Line, Literal_Float => F);
   begin
      Self.Tokens.Append (Tok);
   end Add_Token_Float;

   procedure Add_Token_Character (Self : in out Scanner_Instance; T : Token_Types; Ch : Character)
   is
      Tok : constant Token (CHARACTER_LITERAL) :=
        (Literal_Kind => CHARACTER_LITERAL, Token_Type => T,
         Lexeme       => To_Unbounded_String ("" & Ch), Line => Self.Line, Literal_Char => Ch);
   begin
      Self.Tokens.Append (Tok);
   end Add_Token_Character;

   function Advance (Self : in out Scanner_Instance) return Character is
   begin
      declare
         C : constant Character := Element (Self.Source, Self.Current);
      begin
         Self.Current := Self.Current + 1;

         return C;
      end;
   end Advance;

   function Peek (Self : Scanner_Instance) return Character is
   begin
      if Is_At_End (Self) then
         return ASCII.NUL;
      end if;

      return Element (Self.Source, Self.Current);
   end Peek;

   function Peek_Next (Self : Scanner_Instance) return Character is
   begin
      if Self.Current + 1 > Length (Self.Source) then
         return ASCII.NUL;
      end if;

      return Element (Self.Source, Self.Current + 1);
   end Peek_Next;

   function Match (Self : in out Scanner_Instance; Ch : Character) return Boolean is
   begin
      if Is_At_End (Self) then
         return True;
      end if;

      if Element (Self.Source, Self.Current) /= Ch then
         return False;
      end if;

      Self.Current := Self.Current + 1;
      return True;
   end Match;

   procedure Scan_String (Self : in out Scanner_Instance) is
      Unused : Character;
      Str    : Unbounded_String;
   begin
      while Peek (Self) /= ''' and then not Is_At_End (Self) loop
         if Peek (Self) = ASCII.LF then
            Self.Line := Self.Line + 1;
         end if;

         Unused := Advance (Self);
      end loop;

      if Is_At_End (Self) then
         raise Unexpected_EOF;
      end if;

      Unused := Advance (Self);

      Str := Unbounded_Slice (Self.Source, Self.Start + 1, Self.Current - 2);
      Add_Token_String (Self, STRING_TOKEN, Str);
   end Scan_String;

   procedure Scan_Number (Self : in out Scanner_Instance) is
      Unused   : Character;
      Is_Float : Boolean := False;
   begin
      --  TODO: Implement fixed point numbers (ScaledDecimalLiteral)
      --  Ada does not have any predefined fixed point types

      while Is_Digit (Peek (Self)) loop
         Unused := Advance (Self);
      end loop;

      --  look for a fractional part
      if Peek (Self) = '.' and then Is_Digit (Peek_Next (Self)) then
         --  consume the "."
         Unused := Advance (Self);

         while Is_Digit (Peek (Self)) loop
            Unused := Advance (Self);
         end loop;

         Is_Float := True;
      end if;

      if Peek (Self) = 'r'
        and then (Is_Alpha (Peek_Next (Self)) or else Is_Digit (Peek_Next (Self)))
      then
         --  This number has a specified radix, like 16r12345678
         --  or 16rF000 or 8r300 or 2r1010
         declare
            Radix_Str         : constant Unbounded_String :=
              Unbounded_Slice (Self.Source, Self.Start, Self.Current - 1);
            Start_After_Radix : Positive;
            Value_Str         : Unbounded_String;
         begin
            --  consume the "r"
            Unused := Advance (Self);

            Start_After_Radix := Self.Current;

            while Is_Alpha (Peek (Self)) or else Is_Digit (Peek (Self)) loop
               Unused := Advance (Self);
            end loop;

            Value_Str := Unbounded_Slice (Self.Source, Start_After_Radix, Self.Current - 1);

            Add_Token_Integer
              (Self, NUMBER_TOKEN,
               Long_Integer'Value (To_String (Radix_Str & "#" & Value_Str & "#")));
         end;

         return;
      end if;

      if Peek (Self) = 'e' and then (Is_Digit (Peek_Next (Self)) or else Peek_Next (Self) = '-')
      then
         --  Exponent
         --  consume the "e"
         Unused := Advance (Self);

         if Peek (Self) = '-' then
            --  negative exponent, consume the -
            Unused := Advance (Self);
         end if;

         while Is_Digit (Peek (Self)) loop
            Unused := Advance (Self);
         end loop;
      end if;

      declare
         S : constant String :=
           To_String (Unbounded_Slice (Self.Source, Self.Start, Self.Current - 1));
      begin
         if Is_Float then
            Add_Token_Float (Self, NUMBER_TOKEN, Long_Float'Value (S));
         else
            Add_Token_Integer (Self, NUMBER_TOKEN, Long_Integer'Value (S));
         end if;
      end;
   end Scan_Number;

   procedure Scan_Identifier (Self : in out Scanner_Instance) is
      Unused     : Character;
      Str        : Unbounded_String;
      Is_Keyword : Boolean := False;
   begin
      while Is_Alphanumeric (Peek (Self)) or else Peek (Self) = '_' loop
         Unused := Advance (Self);
      end loop;

      --  Check if this ends with :, if so, it's a keyword
      if Peek (Self) = ':' then
         Is_Keyword := True;

         Unused := Advance (Self);
      end if;

      Str := Unbounded_Slice (Self.Source, Self.Start, Self.Current - 1);

      --  look for keywords

      if Str = "thisContext" then
         Add_Token (Self, THISCONTEXT_TOKEN);
      elsif Str = "self" then
         Add_Token (Self, SELF_TOKEN);
      elsif Str = "super" then
         Add_Token (Self, SUPER_TOKEN);
      elsif Str = "nil" then
         Add_Token (Self, NIL_TOKEN);
      elsif Str = "false" then
         Add_Token (Self, FALSE_TOKEN);
      elsif Str = "true" then
         Add_Token (Self, TRUE_TOKEN);
      elsif Is_Keyword then
         Add_Token (Self, KEYWORD);
      else
         Add_Token (Self, IDENTIFIER);
      end if;
   end Scan_Identifier;

   function Is_At_End (Self : Scanner_Instance) return Boolean is
   begin
      return Self.Current > Length (Self.Source);
   end Is_At_End;

   function Is_Digit (Ch : Character) return Boolean is
   begin
      if Ch >= '0' and then Ch <= '9' then
         return True;
      end if;

      return False;
   end Is_Digit;

   function Is_Alpha (Ch : Character) return Boolean is
   begin
      if (Ch >= 'a' and then Ch <= 'z') or else (Ch >= 'A' and then Ch <= 'Z') or else Ch = '_' then
         return True;
      end if;

      return False;
   end Is_Alpha;

   function Is_Alphanumeric (Ch : Character) return Boolean is
   begin
      return Is_Alpha (Ch) or else Is_Digit (Ch);
   end Is_Alphanumeric;

   function Is_Binary_Selector_Char (Ch : Character) return Boolean is
      Result : Boolean := False;
   begin
      case Ch is
         when '~' | '!' | '@' | '%' | '&' | '*' | '-' | '+' | '=' | '|' | '\' | '<' | '>' | ','
           | '?' | '/' =>
            Result := True;
         when others =>
            Result := False;
      end case;

      return Result;
   end Is_Binary_Selector_Char;
end Plaid.St.Scanner;
