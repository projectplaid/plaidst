with Ada.Containers;
with Ada.Containers.Vectors;

with Ada.Strings;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package Plaid.St.Scanner is
   pragma Elaborate_Body;

   --  Working off "Formal EBNF Specification of Smalltalk System"
   --  From http://cs434.cs.ua.edu/spring2021/project-syntax.pdf

   Unexpected_EOF  : exception;
   Unexpected_Char : exception;

   type Token_Types is
     (LEFT_PAREN, RIGHT_PAREN, LEFT_BRACE, RIGHT_BRACE, DOT, SEMICOLON, SLASH, AT_SYMBOL, HASH,
      COLON, CARET, IDENTIFIER, STRING_TOKEN, NUMBER_TOKEN, SELF_TOKEN, THISCONTEXT_TOKEN,
      SUPER_TOKEN, NIL_TOKEN, FALSE_TOKEN, TRUE_TOKEN, KEYWORD, BINARY_SELECTOR, CHARACTER_TOKEN,
      EOF);

   type Literal_Types is
     (NO_LITERAL, STRING_LITERAL, INTEGER_LITERAL, FLOAT_LITERAL, CHARACTER_LITERAL);

   type Token (Literal_Kind : Literal_Types := NO_LITERAL) is record
      Token_Type : Token_Types;
      Lexeme     : Unbounded_String;
      Line       : Natural;
      case Literal_Kind is
         when STRING_LITERAL =>
            Literal_String : Unbounded_String;
         when INTEGER_LITERAL =>
            Literal_Int : Long_Integer;
         when FLOAT_LITERAL =>
            Literal_Float : Long_Float;
         when CHARACTER_LITERAL =>
            Literal_Char : Character;
         when NO_LITERAL =>
            null;
      end case;
   end record;

   package Token_Vectors is new Ada.Containers.Vectors
     (Index_Type => Natural, Element_Type => Token);

   type Scanner_Instance is private;

   function Token_Img (T : Token) return String;

   function New_Scanner (Source : Unbounded_String) return Scanner_Instance;
   procedure Scan_Tokens (Self : in out Scanner_Instance);
   function Tokens (Self : Scanner_Instance) return Token_Vectors.Vector;

private

   type Scanner_Instance is record
      Tokens  : Token_Vectors.Vector;
      Source  : Unbounded_String;
      Start   : Natural;
      Current : Natural;
      Line    : Positive;
   end record;

   type Radix is range 2 .. 16;

   procedure Scan_Token (Self : in out Scanner_Instance);
   procedure Add_Token (Self : in out Scanner_Instance; T : Token_Types);
   procedure Add_Token_Integer (Self : in out Scanner_Instance; T : Token_Types; I : Long_Integer);
   procedure Add_Token_Float (Self : in out Scanner_Instance; T : Token_Types; F : Long_Float);
   procedure Add_Token_String
     (Self : in out Scanner_Instance; T : Token_Types; Str : Unbounded_String);
   procedure Add_Token_Character (Self : in out Scanner_Instance; T : Token_Types; Ch : Character);

   function Advance (Self : in out Scanner_Instance) return Character;
   function Match (Self : in out Scanner_Instance; Ch : Character) return Boolean;
   function Peek (Self : Scanner_Instance) return Character;
   function Peek_Next (Self : Scanner_Instance) return Character;
   procedure Scan_String (Self : in out Scanner_Instance);
   procedure Scan_Number (Self : in out Scanner_Instance);
   procedure Scan_Identifier (Self : in out Scanner_Instance);

   function Is_Binary_Selector_Char (Ch : Character) return Boolean;

   function Is_Digit (Ch : Character) return Boolean;
   function Is_Alpha (Ch : Character) return Boolean;
   function Is_Alphanumeric (Ch : Character) return Boolean;
   function Is_At_End (Self : Scanner_Instance) return Boolean;

end Plaid.St.Scanner;
