with Plaid.St.Scanner; use Plaid.St.Scanner;

package Plaid.St.Ast.Expr is

   type Expr is abstract tagged null record;
   type Expr_Access is access Expr;

   type Binary is new Expr with record
      Left     : Expr_Access;
      Operator : Token;
      Right    : Expr_Access;
   end record;
   type Binary_Access is access Binary;

   procedure Create_Binary
     (Left : Expr_Access; Operator : Token; Right : Expr_Access; Result : out Binary_Access);

   type Unary is new Expr with record
      Operator : Token;
      Right    : Expr_Access;
   end record;
   type Unary_Access is access Unary;

   procedure Create_Unary (Operator : Token; Right : Expr_Access; Result : out Unary_Access);

   type Literal is new Expr with record
      Value : St_Object_Access;
   end record;
   type Literal_Access is access Literal;

   procedure Create_Literal (Value : St_Object_Access; Result : out Literal_Access);

   type Grouping is new Expr with record
      Expression : Expr_Access;
   end record;
   type Grouping_Access is access Grouping;

   procedure Create_Grouping (Expression : Expr_Access; Result : out Grouping_Access);

end Plaid.St.Ast.Expr;
