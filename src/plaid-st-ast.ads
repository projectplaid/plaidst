with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package Plaid.St.Ast is
   pragma Elaborate_Body;

   type Node is abstract tagged limited null record;
   type Node_Ptr is access Node'Class;

   procedure Accept_Node (Item : Node) is abstract;
   procedure Visit_Node (Item : Node) is abstract;

   type Literal is new Node with record
      --  add source location stuff here?
      Value : Unbounded_String;
   end record;
   overriding procedure Accept_Node (Item : Literal);
   overriding procedure Visit_Node (Item : Literal);

end Plaid.St.Ast;
