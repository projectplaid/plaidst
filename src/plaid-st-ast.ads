package Plaid.St.Ast is
   pragma Elaborate_Body;

   type St_Object is abstract tagged null record;
   type St_Object_Access is access St_Object;
end Plaid.St.Ast;
