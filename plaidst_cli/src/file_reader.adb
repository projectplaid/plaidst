--  Taken from AdaYaml, its copyright notice below

--  part of AdaYaml, (c) 2017 Felix Krause
--  released under the terms of the MIT license, see the file "copying.txt"

with Ada.Directories;
with Ada.Direct_IO;

package body File_Reader is
   function File_Content (Path : String) return Unbounded_String is
      File_Size : constant Natural := Natural (Ada.Directories.Size (Path));

      subtype File_String is String (1 .. File_Size);
      package File_String_IO is new Ada.Direct_IO (File_String);

      File : File_String_IO.File_Type;
   begin
      File_String_IO.Open (File, File_String_IO.In_File, Path);
      declare
         Contents : File_String;
      begin
         File_String_IO.Read (File, Contents);
         File_String_IO.Close (File);

         return To_Unbounded_String (Contents);
      end;
   end File_Content;
end File_Reader;
