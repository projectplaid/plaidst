with Ada.Command_Line;      use Ada.Command_Line;
with Ada.Strings;           use Ada.Strings;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Text_IO;           use Ada.Text_IO;

with File_Reader;
with Plaid;
with Plaid.St;
with Plaid.St.Scanner; use Plaid.St.Scanner;

procedure Plaidst_Cli is

   procedure Run (Source : Unbounded_String) is
      Scanner : Scanner_Instance := New_Scanner (Source);
   begin
      Scan_Tokens (Scanner);

      for T of Tokens (Scanner) loop
         Put_Line (T'Image);
      end loop;

   end Run;

   procedure Run_File (File_Name : String) is
      Source : constant Unbounded_String :=
        File_Reader.File_Content (File_Name);
   begin
      Run (Source);
   end Run_File;

   procedure Run_Transcript is
   begin
      loop
         Put ("> ");
         declare
            Input : constant String := Get_Line (Standard_Input);
         begin
            if Input = "" then
               return;
            end if;

            Run (To_Unbounded_String (Input));
         end;
      end loop;
   end Run_Transcript;

begin
   if Argument_Count > 1 then
      raise Constraint_Error with "Usage: plaidst_cli [source file]";
   elsif Argument_Count = 1 then
      Run_File (Argument (1));
   else
      Run_Transcript;
   end if;
end Plaidst_Cli;
