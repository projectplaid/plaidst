with Ada.Strings;           use Ada.Strings;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package File_Reader is
   function File_Content (Path : String) return Unbounded_String;
end File_Reader;
