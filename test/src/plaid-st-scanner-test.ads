with Ada.Containers.Indefinite_Ordered_Maps;

with AUnit;            use AUnit;
with AUnit.Test_Cases; use AUnit.Test_Cases;

package Plaid.St.Scanner.Test is
   type TC is new Test_Cases.Test_Case with record
      null;
   end record;

   use type Token_Vectors.Vector;

   package Source_Expected_Map is new Ada.Containers.Indefinite_Ordered_Maps
     (Key_Type => String, Element_Type => Token_Vectors.Vector);

   overriding procedure Register_Tests (T : in out TC);
   overriding procedure Set_Up (T : in out TC);

   overriding function Name (T : TC) return Message_String;

   procedure Scan_Class_Definition (T : in out Test_Cases.Test_Case'Class);
   procedure Scan_Numbers (T : in out Test_Cases.Test_Case'Class);
   procedure Scan_Binary_Selector_Char (T : in out Test_Cases.Test_Case'Class);
   procedure Scan_Character_Literals (T : in out Test_Cases.Test_Case'Class);

private

   function Create_Token (Token_Type : Token_Types; Lexeme : String; Line : Positive) return Token;
   function Create_Token_Str
     (Token_Type : Token_Types; Lexeme : String; Line : Positive) return Token;
   function Create_Token_Integer
     (Token_Type : Token_Types; Lexeme : String; Line : Positive; Value : Long_Integer)
      return Token;
   function Create_Token_Float
     (Token_Type : Token_Types; Lexeme : String; Line : Positive; Value : Long_Float) return Token;
   function Create_Token_Char
     (Token_Type : Token_Types; Lexeme : String; Line : Positive; Value : Character) return Token;
end Plaid.St.Scanner.Test;
