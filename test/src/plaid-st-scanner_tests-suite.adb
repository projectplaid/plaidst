with Plaid.St.Scanner.Test;

package body Plaid.St.Scanner_Tests.Suite is
   Result     : aliased AUnit.Test_Suites.Test_Suite;
   Scanner_TC : aliased Plaid.St.Scanner.Test.TC;

   function Suite return AUnit.Test_Suites.Access_Test_Suite is
   begin
      AUnit.Test_Suites.Add_Test (Result'Access, Scanner_TC'Access);
      return Result'Access;
   end Suite;
end Plaid.St.Scanner_Tests.Suite;
