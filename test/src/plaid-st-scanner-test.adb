with Ada.Strings;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

with AUnit.Assertions; use AUnit.Assertions;

with Ada.Text_IO; use Ada.Text_IO;

with Utils;

package body Plaid.St.Scanner.Test is
   overriding procedure Register_Tests (T : in out TC) is
      use AUnit.Test_Cases.Registration;
   begin
      Register_Routine (T, Scan_Numbers'Access, "Scan numbers");
      Register_Routine (T, Scan_Character_Literals'Access, "Scan character literals");
      Register_Routine (T, Scan_Binary_Selector_Char'Access, "Scan binary selector chars");
      Register_Routine (T, Scan_Class_Definition'Access, "Scan class definition");
   end Register_Tests;

   overriding procedure Set_Up (T : in out TC) is
   begin
      null;
   end Set_Up;

   overriding function Name (T : TC) return Message_String is
      pragma Unreferenced (T);
   begin
      return AUnit.Format ("Smalltalk scanner");
   end Name;

   procedure Scan_Class_Definition (T : in out Test_Cases.Test_Case'Class) is
      pragma Unreferenced (T);
      use Token_Vectors;

      Data_Path : constant String := "test/data/tokens_scanner.st";
      Source    : constant String := Utils.File_Content (Data_Path);
   begin
      declare
         Scanner_Inst : Scanner_Instance := New_Scanner (To_Unbounded_String (Source));
      begin
         Plaid.St.Scanner.Scan_Tokens (Scanner_Inst);
         declare
            Actual : constant Token_Vectors.Vector := Tokens (Scanner_Inst);

            Expected : constant Plaid.St.Scanner.Token_Vectors.Vector :=
              [Create_Token (IDENTIFIER, "ProtoObject", 1), Create_Token (KEYWORD, "subclass:", 1),
              Create_Token (HASH, "#", 1), Create_Token (IDENTIFIER, "Object", 1),

              Create_Token (KEYWORD, "instanceVariableNames:", 2),
              Create_Token_Str (STRING_TOKEN, "", 2),

              Create_Token (KEYWORD, "classVariableNames:", 3),
              Create_Token_Str (STRING_TOKEN, "DependentsFields", 3),

              Create_Token (KEYWORD, "poolDictionaries:", 4),
              Create_Token_Str (STRING_TOKEN, "", 4), Create_Token (KEYWORD, "category:", 5),
              Create_Token_Str (STRING_TOKEN, "Kernel-Objects", 5), Create_Token (DOT, ".", 5),

              Create_Token (EOF, "", 6)];
         begin
            Assert (Expected = Actual, "Expected should equal actual");
         end;
      end;
   end Scan_Class_Definition;

   procedure Scan_Numbers (T : in out Test_Cases.Test_Case'Class) is
      pragma Unreferenced (T);
      use Token_Vectors;
      use Source_Expected_Map;

      Fixtures : Source_Expected_Map.Map;
   begin
      Fixtures.Insert
        ("1.",
         [Create_Token_Integer (NUMBER_TOKEN, "", 1, 1), Create_Token (DOT, ".", 1),
         Create_Token (EOF, "", 1)]);

      Fixtures.Insert
        ("1.2345.",
         [Create_Token_Float (NUMBER_TOKEN, "", 1, 1.234_5), Create_Token (DOT, ".", 1),
         Create_Token (EOF, "", 1)]);

      Fixtures.Insert
        ("-1.",
         [Create_Token_Integer (NUMBER_TOKEN, "", 1, -1), Create_Token (DOT, ".", 1),
         Create_Token (EOF, "", 1)]);

      Fixtures.Insert
        ("-1.2345.",
         [Create_Token_Float (NUMBER_TOKEN, "", 1, -1.234_5), Create_Token (DOT, ".", 1),
         Create_Token (EOF, "", 1)]);

      Fixtures.Insert
        ("16rc0ffeef00d.",
         [Create_Token_Integer (NUMBER_TOKEN, "", 1, 828_927_569_933), Create_Token (DOT, ".", 1),
         Create_Token (EOF, "", 1)]);

      Fixtures.Insert
        ("2.4e7",
         [Create_Token_Float (NUMBER_TOKEN, "", 1, 2.400_000_000_000_00E+07),
         Create_Token (EOF, "", 1)]);

      Fixtures.Insert
        ("2.4e-2",
         [Create_Token_Float (NUMBER_TOKEN, "", 1, 2.400_000_000_000_00E-02),
         Create_Token (EOF, "", 1)]);

      Fixtures.Insert
        ("10e4", [Create_Token_Integer (NUMBER_TOKEN, "", 1, 100_000), Create_Token (EOF, "", 1)]);

      for Fixture in Fixtures.Iterate loop
         declare
            Scanner_Inst : Scanner_Instance := New_Scanner (To_Unbounded_String (Key (Fixture)));
         begin
            Plaid.St.Scanner.Scan_Tokens (Scanner_Inst);
            declare
               Actual : constant Token_Vectors.Vector := Tokens (Scanner_Inst);

               Expected : constant Plaid.St.Scanner.Token_Vectors.Vector := Fixtures (Fixture);
            begin
               Assert
                 (Expected = Actual, "Problem lexing " & Key (Fixture) & ": Got " & Actual'Image);
            end;
         end;
      end loop;
   end Scan_Numbers;

   procedure Scan_Binary_Selector_Char (T : in out Test_Cases.Test_Case'Class) is
      pragma Unreferenced (T);
      use Token_Vectors;
   begin
      declare
         Scanner_Inst : Scanner_Instance := New_Scanner (To_Unbounded_String ("~~ ."));
      begin
         Plaid.St.Scanner.Scan_Tokens (Scanner_Inst);
         declare
            Actual : constant Token_Vectors.Vector := Tokens (Scanner_Inst);

            Expected : constant Plaid.St.Scanner.Token_Vectors.Vector :=
              [Create_Token (BINARY_SELECTOR, "~~", 1), Create_Token (DOT, ".", 1),
              Create_Token (EOF, "", 1)];
         begin
            Assert (Expected = Actual, "Expected should equal actual");
         end;
      end;
   end Scan_Binary_Selector_Char;

   procedure Scan_Character_Literals (T : in out Test_Cases.Test_Case'Class) is
      pragma Unreferenced (T);
      use Token_Vectors;
      use Source_Expected_Map;

      Fixtures : Source_Expected_Map.Map;
   begin
      Fixtures.Insert
        ("$a.",
         [Create_Token_Char (CHARACTER_TOKEN, "a", 1, 'a'), Create_Token (DOT, ".", 1),
         Create_Token (EOF, "", 1)]);

      Fixtures.Insert
        ("$.", [Create_Token_Char (CHARACTER_TOKEN, ".", 1, '.'), Create_Token (EOF, "", 1)]);

      Fixtures.Insert
        ("$$.",
         [Create_Token_Char (CHARACTER_TOKEN, "$", 1, '$'), Create_Token (DOT, ".", 1),
         Create_Token (EOF, "", 1)]);

      for Fixture in Fixtures.Iterate loop
         declare
            Scanner_Inst : Scanner_Instance := New_Scanner (To_Unbounded_String (Key (Fixture)));
         begin
            Plaid.St.Scanner.Scan_Tokens (Scanner_Inst);
            declare
               Actual : constant Token_Vectors.Vector := Tokens (Scanner_Inst);

               Expected : constant Plaid.St.Scanner.Token_Vectors.Vector := Fixtures (Fixture);
            begin
               Assert
                 (Expected = Actual, "Problem lexing " & Key (Fixture) & ": Got " & Actual'Image);
            end;
         end;
      end loop;
   end Scan_Character_Literals;

   function Create_Token (Token_Type : Token_Types; Lexeme : String; Line : Positive) return Token
   is
      Tok : constant Token (NO_LITERAL) :=
        (Literal_Kind => NO_LITERAL, Token_Type => Token_Type,
         Lexeme       => To_Unbounded_String (Lexeme), Line => Line);
   begin
      return Tok;
   end Create_Token;

   function Create_Token_Str
     (Token_Type : Token_Types; Lexeme : String; Line : Positive) return Token
   is
      Tok : constant Token (STRING_LITERAL) :=
        (Literal_Kind   => STRING_LITERAL, Token_Type => Token_Type,
         Lexeme         => To_Unbounded_String (Lexeme), Line => Line,
         Literal_String => To_Unbounded_String (Lexeme));
   begin
      return Tok;
   end Create_Token_Str;

   function Create_Token_Integer
     (Token_Type : Token_Types; Lexeme : String; Line : Positive; Value : Long_Integer) return Token
   is
      Tok : constant Token (INTEGER_LITERAL) :=
        (Literal_Kind => INTEGER_LITERAL, Token_Type => Token_Type,
         Lexeme       => To_Unbounded_String (Lexeme), Line => Line, Literal_Int => Value);
   begin
      return Tok;
   end Create_Token_Integer;

   function Create_Token_Float
     (Token_Type : Token_Types; Lexeme : String; Line : Positive; Value : Long_Float) return Token
   is
      Tok : constant Token (FLOAT_LITERAL) :=
        (Literal_Kind => FLOAT_LITERAL, Token_Type => Token_Type,
         Lexeme       => To_Unbounded_String (Lexeme), Line => Line, Literal_Float => Value);
   begin
      return Tok;
   end Create_Token_Float;

   function Create_Token_Char
     (Token_Type : Token_Types; Lexeme : String; Line : Positive; Value : Character) return Token
   is
      Tok : constant Token (CHARACTER_LITERAL) :=
        (Literal_Kind => CHARACTER_LITERAL, Token_Type => Token_Type,
         Lexeme       => To_Unbounded_String (Lexeme), Line => Line, Literal_Char => Value);
   begin
      return Tok;
   end Create_Token_Char;
end Plaid.St.Scanner.Test;
