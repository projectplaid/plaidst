with AUnit.Run;
with AUnit.Reporter.Text;

with Plaid.St.Scanner_Tests.Suite;

procedure Plaid.St.Scanner_Tests.Harness is
   procedure Run is new AUnit.Run.Test_Runner (Suite.Suite);
   Reporter : AUnit.Reporter.Text.Text_Reporter;
begin
   Reporter.Set_Use_ANSI_Colors (True);
   Run (Reporter);
end Plaid.St.Scanner_Tests.Harness;
