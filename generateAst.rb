#!/usr/bin/env ruby
# frozen_string_literal: true

require 'erb'
require 'ostruct'

# rubocop:todo all
$ads_template = %q{
with Plaid.St.Scanner; use Plaid.St.Scanner;

package Plaid.St.Ast.<%= base_name %> is

   type <%= base_name %> is abstract tagged null record;
   type <%= base_name %>_Access is access <%= base_name %>;

% types.each do |typ|
   type <%= typ[0] %> is new <%= base_name %> with record
% typ[1].each do |t|
     <%= t %>;
% end
   end record;
   type <%= typ[0] %>_Access is access <%= typ[0] %>;

   procedure Create_<%= typ[0] %> (
% typ[1].each do |t|
     <%= t %>;
% end
     Result : out <%= typ[0] %>_Access);

% end

end Plaid.St.Ast.<%= base_name %>;
}.gsub(/^s+/, '')

$adb_template = %q{
package body Plaid.St.Ast.<%= base_name %> is

% types.each do |typ|
   procedure Create_<%= typ[0] %> (
% typ[1].each do |t|
     <%= t %>;
% end
     Result : out <%= typ[0] %>_Access) is
  begin
     Result := new <%= typ[0] %>;
% typ[1].each do |t|
     Result.<%= t.split(':')[0] %> := <%= t.split(':')[0] %>;
% end
  end Create_<%= typ[0] %>;

% end

end Plaid.St.Ast.<%= base_name %>;
}.gsub(/^s+/, '')
# rubocop:enable all

def define_ast(dest_folder, base_name, types)
  File.open("#{dest_folder}/plaid-st-ast-#{base_name.downcase}.ads", 'w') do |file|
    ads = ERB.new($ads_template, trim_mode: '%<>')
    namespace = OpenStruct.new(base_name: base_name, types: types)
    file.write(ads.result(namespace.instance_eval { binding }))
  end

  File.open("#{dest_folder}/plaid-st-ast-#{base_name.downcase}.adb", 'w') do |file|
    adb = ERB.new($adb_template, trim_mode: '%<>')
    namespace = OpenStruct.new(base_name: base_name, types: types)
    file.write(adb.result(namespace.instance_eval { binding }))
  end
end

abort('specify an output directory on the command line') if ARGV.empty?

dest_folder = ARGV[0]

define_ast(dest_folder,
           'Expr',
           [['Binary', ['Left: Expr_Access', 'Operator: Token', 'Right: Expr_Access']],
            ['Unary', ['Operator: Token', 'Right: Expr_Access']],
            ['Literal', ['Value: St_Object_Access']],
            ['Grouping', ['Expression: Expr_Access']]])
